import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CanineHealthComponent } from './canine-health.component';

describe('CanineHealthComponent', () => {
  let component: CanineHealthComponent;
  let fixture: ComponentFixture<CanineHealthComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CanineHealthComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CanineHealthComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
