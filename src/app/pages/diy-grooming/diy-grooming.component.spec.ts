import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DiyGroomingComponent } from './diy-grooming.component';

describe('DiyGroomingComponent', () => {
  let component: DiyGroomingComponent;
  let fixture: ComponentFixture<DiyGroomingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DiyGroomingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DiyGroomingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
