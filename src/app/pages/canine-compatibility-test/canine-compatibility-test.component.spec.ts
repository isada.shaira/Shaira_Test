import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CanineCompatibilityTestComponent } from './canine-compatibility-test.component';

describe('CanineCompatibilityTestComponent', () => {
  let component: CanineCompatibilityTestComponent;
  let fixture: ComponentFixture<CanineCompatibilityTestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CanineCompatibilityTestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CanineCompatibilityTestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
