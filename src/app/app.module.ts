import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

//modules for routing
import { AppRoutingModule } from './app-routing.module';
import { RouterModule, Routes } from '@angular/router';

//3rd party imports for material/animations/flex etc.
import { ImportsModule } from './imports.module';

//page components
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { MenuComponent } from './menu/menu.component';
import { FooterComponent } from './footer/footer.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { HomeComponent } from './pages/home/home.component'
import { CanineListComponent } from './pages/canine-list/canine-list.component';
import { CanineHealthComponent } from './pages/canine-health/canine-health.component';
import { CanineCompatibilityTestComponent } from './pages/canine-compatibility-test/canine-compatibility-test.component';
import { DiyGroomingComponent } from './pages/diy-grooming/diy-grooming.component';
import { ForumComponent } from './pages/forum/forum.component';



const appRoutes: Routes = [
  { path: 'home', component: HomeComponent},
  { path: '', redirectTo: '/home', pathMatch: 'full'},
  { path: '', redirectTo: '/home', pathMatch: 'full'},
  { path: 'logout', component: HomeComponent},
  { path: 'login', component: LoginComponent},
  { path: 'register', component: RegisterComponent},
  { path: 'canine-list', component: CanineListComponent},
  { path: 'canine-health', component: CanineHealthComponent},
  { path: 'canine-compatibility-test', component: CanineCompatibilityTestComponent},
  { path: 'diy-grooming', component: DiyGroomingComponent},
  { path: 'forum', component: ForumComponent},


  
  { path: '**', redirectTo: '/home', pathMatch: 'full'}
]

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    MenuComponent,
    FooterComponent,
    LoginComponent,
    RegisterComponent,
    CanineListComponent,
    CanineHealthComponent,
    CanineCompatibilityTestComponent,
    DiyGroomingComponent,
    ForumComponent,
    HomeComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    RouterModule.forRoot(
                          appRoutes,
                          { enableTracing: true } // <-- debugging purposes only
                        ),
    ImportsModule

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
