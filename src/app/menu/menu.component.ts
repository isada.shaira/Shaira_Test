import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    this.getMenuItems();
    console.log(this.menuObj);
  }

  menuObj: any = [];
  items: any = [];


  getMenuItems() {
    this.menuObj = [
      {
        'label': 'Home',
        'icon': 'fa fa-fw fa-check',
        'link': '/home'
      },
      {
        'label': 'Canine List',
        'icon': 'fafa-fwfa-download',
        'link': '/canine-list'
      },
      {
        'label': 'Canine Health',
        'icon': 'fafa-fwfa-refresh',
        'link': '/canine-health'
      },
      {
        'label': 'DIY Grooming',
        'icon': 'fafa-fwfa-download',
        'link': '/diy-grooming'
        
      },
      {
        'label': 'Forum',
        'icon': 'fafa-fwfa-download',
        'link': '/forum'
        
      },
    ];
  }

  getMenu(index: number, elem: any) {
    console.log("asdasda. ." + index)
    console.log(elem);
  }
  

}
