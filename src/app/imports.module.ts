import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

// Imports from third party modules

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FlexLayoutModule } from '@angular/flex-layout';

import { MatToolbarModule } from '@angular/material/toolbar';
import { MatMenuModule, MatButtonModule, MatIconModule, MatCardModule } from '@angular/material';
import { MatCheckboxModule,  MatInputModule } from '@angular/material';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatFormFieldModule } from '@angular/material/form-field';


@NgModule({
  declarations: [
   
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FlexLayoutModule,
    MatToolbarModule,
    MatMenuModule, 
    MatButtonModule, 
    MatIconModule, 
    MatCardModule,
    MatGridListModule,
    MatFormFieldModule,
    MatCheckboxModule,  
    MatInputModule

  ],
  exports: [BrowserModule,
    BrowserAnimationsModule,
    FlexLayoutModule,
    MatToolbarModule,
    MatMenuModule, 
    MatButtonModule, 
    MatIconModule, 
    MatCardModule,
    MatGridListModule,
    MatFormFieldModule,
    MatCheckboxModule,  
    MatInputModule ]
})
export class ImportsModule { }
